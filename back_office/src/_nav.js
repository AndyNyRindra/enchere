import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilApps, cilBasket,
  cilBell,
  cilCalculator,
  cilChartPie, cilChevronDoubleDown, cilClock,
  cilCursor,
  cilDescription,
  cilDrop, cilEuro, cilMeh,
  cilNotes,
  cilPencil,
  cilPuzzle,
  cilSpeedometer,
  cilStar
} from "@coreui/icons";
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Login',
    to: '/login',
    icon: <CIcon icon={cilMeh} customClassName="nav-icon" />,

  },
  {
    component: CNavItem,
    name: 'Accueil',
    to: '/accueil',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,

  },
  {
    component: CNavTitle,
    name: 'Gestion',
  },
  {
    component: CNavItem,
    name: 'Catégories',
    to: '/categories',
    icon: <CIcon icon={cilBasket} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Comissions',
    to: '/comissions',
    icon: <CIcon icon={cilEuro} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Durée enchère',
    to: '/dureeEncheres',
    icon: <CIcon icon={cilClock} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'Rechargement',
  },
  {
    component: CNavItem,
    name: 'Demandes',
    to: '/rechargement',
    icon: <CIcon icon={cilChevronDoubleDown} customClassName="nav-icon" />,
  }
]

export default _nav
