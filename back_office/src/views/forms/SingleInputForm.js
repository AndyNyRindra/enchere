import React from 'react'
import { CFormFeedback, CFormInput, CInputGroup, CInputGroupText } from "@coreui/react";


const SingleInputForm = () => {

  return (
    <>

      <CInputGroup className="mb-3">
        <CInputGroupText id="basic-addon1">Valeur</CInputGroupText>
        <CFormInput placeholder="Valeur" aria-label="Valeur" aria-describedby="basic-addon1" id="valeur" name="valeur" required/>
        <CFormFeedback invalid>Veuillez remplir ce champ</CFormFeedback>
      </CInputGroup>
    </>
  )
}

export default SingleInputForm
