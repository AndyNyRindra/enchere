import React from 'react'
import {useState} from 'react';
import {useEffect} from 'react';
import {CButton, CCol, CRow, CTable} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {cilPencil, cilPlus, cilTrash} from "@coreui/icons";
import { Link } from "react-router-dom";


const Accueil = () => {
  const [categoriesRentables, setCategoriesRentables] = useState([]);
  const [categoriesPopulaires, setCategoriesPopulaires] = useState([]);
  const [usersRentables, setUsersRentables] = useState([]);
  const [usersPopulaires, setUsersPopulaires] = useState([]);
  const token = sessionStorage.getItem("admin_token")
  if (token == null) {
    window.location.href = '/login'
  }
  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'admin_token': token
    }
  }
  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/statistique/1', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setCategoriesPopulaires(res.data)
        }
      })
    fetch('https://enchereandy-production.up.railway.app/statistique/2', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setCategoriesRentables(res.data)
        }
      })
    fetch('https://enchereandy-production.up.railway.app/statistique/3', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setUsersPopulaires(res.data)
        }
      })
    fetch('https://enchereandy-production.up.railway.app/statistique/4', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setUsersRentables(res.data)
        }
      })
  }, [])


  const columns = [
    {
      key: 'id',
      label: '#',
      _props: { scope: 'col' },
    },
    {
      key: 'nom',
      label: 'Nom',
      _props: { scope: 'col' },
    },
    {
      key: 'valeur',
      label: 'Valeur',
      _props: { scope: 'col' },
    }
  ]
  const items = []
  const items2 = []
  const items3 = []
  const items4 = []
  function getOneLine(entity) {
    const item = {
      id: entity.id,
      nom: entity.nom,
      valeur: entity.stat,
      _cellProps: { id: { scope: 'row' } },

    }
    items.push(item)
  }
  function getOneLine2(entity) {
    const item = {
      id: entity.id,
      nom: entity.nom,
      valeur: entity.stat,
      _cellProps: { id: { scope: 'row' } },

    }
    items2.push(item)
  }
  function getOneLine3(entity) {
    const item = {
      id: entity.id,
      nom: entity.nom,
      valeur: entity.stat,
      _cellProps: { id: { scope: 'row' } },

    }
    items3.push(item)
  }
  function getOneLine4(entity) {
    const item = {
      id: entity.id,
      nom: entity.nom,
      valeur: entity.stat,
      _cellProps: { id: { scope: 'row' } },

    }
    items4.push(item)
  }
  categoriesPopulaires.map(getOneLine)
  categoriesRentables.map(getOneLine2)
  usersPopulaires.map(getOneLine3)
  usersRentables.map(getOneLine4)
  return (
    <>
      <CRow>
        <CCol><h2>Statistiques</h2></CCol>
      </CRow>
      <CRow>
        <CCol><h4>Top 10 Catégories populaires</h4><CTable striped columns={columns} items={items} /></CCol>
        <CCol><h4>Top 10 Catégories rentables</h4><CTable striped columns={columns} items={items2} /></CCol>
      </CRow>
      <CRow>
        <CCol><h4>Top 10 Utilisateurs populaires</h4><CTable striped columns={columns} items={items3} /></CCol>
        <CCol><h4>Top 10 Utilisateurs rentables</h4><CTable striped columns={columns} items={items4} /></CCol>
      </CRow>

    </>
  )
}

export default Accueil
