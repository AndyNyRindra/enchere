import React from 'react'
import {useState} from 'react';
import {useEffect} from 'react';
import {CButton, CCol, CRow, CTable} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {cilPencil, cilPlus, cilTrash} from "@coreui/icons";
import { Link } from "react-router-dom";


const CategorieList = () => {
  const [categories, setCategories] = useState([]);
  const token = sessionStorage.getItem("admin_token")
  if (token == null) {
    window.location.href = '/login'
  }
  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'admin_token': token
    }
  }
  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/categories', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setCategories(res.data)
        }
      })
  }, [])


  const columns = [
    {
      key: 'id',
      label: '#',
      _props: { scope: 'col' },
    },
    {
      key: 'nom',
      label: 'Nom',
      _props: { scope: 'col' },
    }
  ]
  const items = []
  function getOneLine(categorie) {
    const item = {
      id: categorie.id,
      nom: categorie.nom,
      _cellProps: { id: { scope: 'row' } },

    }
    items.push(item)
  }
  categories.map(getOneLine)
  return (
    <>
      <CRow>
        <CCol><h2>Liste des Catégories</h2></CCol>
        <CCol></CCol>
        <CCol>
          <Link to="/categories/new"><CButton
            color= "success">
            <CIcon icon={cilPlus} className="me-2" />
            Ajouter
          </CButton></Link>
        </CCol>
      </CRow>

      <CTable striped columns={columns} items={items} />
    </>
  )
}

export default CategorieList
