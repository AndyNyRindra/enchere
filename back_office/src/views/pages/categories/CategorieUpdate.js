import React from 'react'
import {CButton, CForm} from "@coreui/react";
import SingleInputForm from "../../forms/SingleInputForm";


const CategorieUpdate = () => {

  return (
    <>
      <h2>Modifier une catégorie</h2>
      <CForm>
        <SingleInputForm />
        <CButton color="primary" className="px-4" type="submit">
          Mettre à jour
        </CButton>
      </CForm>
    </>
  )
}

export default CategorieUpdate
