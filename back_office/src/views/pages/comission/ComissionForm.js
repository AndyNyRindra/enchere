import React, { useState } from "react";
import {CButton, CForm} from "@coreui/react";
import SingleInputForm from "../../forms/SingleInputForm";


const ComissionForm = () => {

  const [validated, setValidated] = useState(false)

  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    } else {
      const valeur = event.target.valeur.value;

      const content = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'admin_token': sessionStorage.getItem("admin_token")
        }
      };
      content.body = JSON.stringify({valeur: valeur})
      fetch('https://enchereandy-production.up.railway.app/comissions', content).then(response => response.json()).then((data) => {
        if (data.code === 401) {
          alert("Please Login");
          window.location.replace('/login')
        } else if (data.code === 400) {
          alert("error");
          event.preventDefault()
          event.stopPropagation()
        }
        else if (data.data != null) {
          window.location.replace('/comissions')
        }
      }).catch(e => {
        event.preventDefault()
        event.stopPropagation()
      })
    }
    setValidated(true)
    event.preventDefault()
    event.stopPropagation()



  }

  return (
    <>
      <h2>Modifier la comission</h2>
      <CForm onSubmit={handleSubmit} noValidate
             validated={validated} method="POST">
        <SingleInputForm />
        <CButton color="primary" className="px-4" type="submit">
          Mettre à jour
        </CButton>
      </CForm>
    </>
  )
}

export default ComissionForm
