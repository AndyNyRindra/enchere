import React from 'react'
import {useState} from 'react';
import {useEffect} from 'react';
import {CButton, CCol, CRow, CTable} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {cilPlus} from "@coreui/icons";
import { Link } from "react-router-dom";


const ComissionList = () => {
  const [comissions, setComissions] = useState([]);
  const token = sessionStorage.getItem("admin_token")
  if (token == null) {
    window.location.href = '/login'
  }
  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'admin_token': sessionStorage.getItem("admin_token")
    }
  }
  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/comissions', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setComissions(res.data)
        }
      })
  }, [])


  const columns = [
    {
      key: 'id',
      label: '#',
      _props: { scope: 'col' },
    },
    {
      key: 'valeur',
      label: 'Valeur',
      _props: { scope: 'col' },
    },
    {
      key: 'date',
      label: 'Date de création',
      _props: { scope: 'col' },
    }
  ]
  const items = []
  function getOneLine(comission) {
    const item = {
      id: comission.id,
      valeur: comission.valeur + '%',
      date: new Date(comission.date).toLocaleDateString(),
      _cellProps: { id: { scope: 'row' } }
    }
    items.push(item)
  }
  comissions.map(getOneLine)
  return (
    <>
      <CRow>
        <CCol><h2>Liste des comissions</h2></CCol>
        <CCol></CCol>
        <CCol>
          <Link to="/comissions/new"><CButton
            color= "success">
            <CIcon icon={cilPlus} className="me-2" />
            Ajouter
          </CButton></Link>
        </CCol>
      </CRow>

      <CTable striped columns={columns} items={items} />
    </>
  )
}

export default ComissionList
