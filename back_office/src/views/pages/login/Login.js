import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm, CFormFeedback,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'

const Login = () => {
  const [validated, setValidated] = useState(false)

  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    } else {

      const email = event.target.email.value;
      const password = event.target.password.value;

      const content = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      };

      content.body = JSON.stringify({email: email, mdp: password})
      fetch('https://enchereandy-production.up.railway.app/admins/login', content).then(response => response.json()).then((data) => {
        event.preventDefault()
        event.stopPropagation()
        console.log(data)
        if (data.code === 401) {
          alert("error");
          event.preventDefault()
          event.stopPropagation()
        } else if (data.data != null) {
          mapToHome(data.data.value)
        }
      }).catch(e => {
        event.preventDefault()
        event.stopPropagation()
      })
    }
    setValidated(true)
    event.preventDefault()
    event.stopPropagation()

  }

  function mapToHome(token) {
    sessionStorage.setItem("admin_token", token)
    window.location.replace('/accueil')
  }


  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={handleSubmit} noValidate
                         validated={validated} method="POST">
                    <h1>Login</h1>
                    <p className="text-medium-emphasis">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser}/>
                      </CInputGroupText>
                      <CFormInput id="email" name="email" placeholder="Email" autoComplete="email" defaultValue="jean@gmail.com" required/>
                      <CFormFeedback invalid>Please fill the email</CFormFeedback>
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked}/>
                      </CInputGroupText>
                      <CFormInput
                        id="password"
                        name="password"
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                        defaultValue="12345"
                        required
                      />
                      <CFormFeedback invalid>Please fill the password</CFormFeedback>
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton color="primary" className="px-4" type="submit">
                          Login
                        </CButton>
                      </CCol>
                      <CCol xs={6} className="text-right">

                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>

            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
