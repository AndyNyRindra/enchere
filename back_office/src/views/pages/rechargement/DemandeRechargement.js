import React from 'react'
import {useState} from 'react';
import {useEffect} from 'react';
import {CButton, CCol, CRow, CTable} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {cilCheckAlt, cilX } from "@coreui/icons";


const DemandeRechargement = () => {
  const [demandes, setDemandes] = useState([]);
  const token = sessionStorage.getItem("admin_token")

  function accepter(id) {
    console.log("accepted")
    var content = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'admin_token': sessionStorage.getItem("admin_token")
      }
    }
    fetch('https://enchereandy-production.up.railway.app/rechargement/' + id, content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          window.location.reload()
        }
      })
  }

  function refuser(id) {
    console.log("refused")
    var content = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'admin_token': sessionStorage.getItem("admin_token")
      }
    }
    fetch('https://enchereandy-production.up.railway.app/rechargement/' + id, content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          window.location.reload()
        }
      })
  }

  if (token == null) {
    window.location.href = '/login'
  }
  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'admin_token': token
    }
  }
  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/rechargement', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setDemandes(res.data)
        }
      })
  }, [])


  const columns = [
    {
      key: 'id',
      label: '#',
      _props: { scope: 'col' },
    },
    {
      key: 'user',
      label: 'Utilisateur',
      _props: { scope: 'col' },
    },
    {
      key: 'montant',
      label: 'Montant',
      _props: { scope: 'col' },
    },
    {
      key: 'date',
      label: 'Date',
      _props: { scope: 'col' },
    },
    {
      key: 'button1',
      label: '',
      _props: { scope: 'col' },
    },
    {
      key: 'button2',
      label: '',
      _props: { scope: 'col' },
    }
  ]
  const items = []
  function getOneLine(demande) {
    const item = {
      id: demande.id,
      nom: demande.user.nom,
      montant: demande.montant,
      date: new Date(demande.date).toLocaleDateString(),
      button1: <CButton
        color= "success" onClick={() => accepter(demande.id)}>
        <CIcon icon={cilCheckAlt} className="me-2" />
        Accepter
      </CButton>,
      button2: <CButton
        color= "danger" onClick={() => refuser(demande.id)}>
        <CIcon icon={cilX} className="me-2" />
        Refuser
      </CButton>,
      _cellProps: { id: { scope: 'row' } },

    }
    items.push(item)
  }
  demandes.map(getOneLine)
  return (
    <>
      <CRow>
        <CCol><h2>Liste des demandes de rechargement</h2></CCol>

      </CRow>

      <CTable striped columns={columns} items={items} />
    </>
  )
}

export default DemandeRechargement
