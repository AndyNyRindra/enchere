CREATE SEQUENCE "public".admin_0_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".admin_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".admin_token_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".category_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".commission_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".demande_rechargement_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".duree_defaut_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".mise_enchere_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".mouvement_compte_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".photos_enchere_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".produit_enchere_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".type_mouvement_id_seq START WITH 1 INCREMENT BY 1;

CREATE  TABLE "public"."admin" ( 
	id                   integer DEFAULT nextval('admin_id_seq'::regclass) NOT NULL  ,
	nom                  varchar  NOT NULL  ,
	email                varchar  NOT NULL  ,
	mdp                  varchar  NOT NULL  ,
	CONSTRAINT pk_tbl PRIMARY KEY ( id )
 );

CREATE  TABLE "public".admin_token ( 
	id                   integer DEFAULT nextval('admin_token_id_seq'::regclass) NOT NULL  ,
	admin_id             integer  NOT NULL  ,
	"value"              varchar(255)  NOT NULL  ,
	date_expiration      timestamp  NOT NULL  ,
	CONSTRAINT pk_admin_token PRIMARY KEY ( id )
 );

CREATE  TABLE "public".categorie ( 
	id                   integer DEFAULT nextval('category_id_seq'::regclass) NOT NULL  ,
	nom                  varchar  NOT NULL  ,
	CONSTRAINT pk_category PRIMARY KEY ( id )
 );

CREATE  TABLE "public".comission ( 
	id                   integer DEFAULT nextval('commission_id_seq'::regclass) NOT NULL  ,
	valeur               real  NOT NULL  ,
	"date"               date  NOT NULL  ,
	CONSTRAINT pk_commission PRIMARY KEY ( id )
 );

CREATE  TABLE "public".duree_defaut ( 
	id                   integer DEFAULT nextval('duree_defaut_id_seq'::regclass) NOT NULL  ,
	duree                real  NOT NULL  ,
	"date"               date  NOT NULL  ,
	CONSTRAINT pk_duree_defaut PRIMARY KEY ( id )
 );

CREATE  TABLE "public".type_mouvement ( 
	id                   integer DEFAULT nextval('type_mouvement_id_seq'::regclass) NOT NULL  ,
	"type"               varchar  NOT NULL  ,
	CONSTRAINT pk_type_mouvement PRIMARY KEY ( id )
 );

CREATE  TABLE "public"."user" ( 
	id                   integer DEFAULT nextval('admin_0_id_seq'::regclass) NOT NULL  ,
	nom                  varchar  NOT NULL  ,
	email                varchar  NOT NULL  ,
	mdp                  varchar  NOT NULL  ,
	CONSTRAINT pk_tbl_0 PRIMARY KEY ( id )
 );

CREATE  TABLE "public".demande_rechargement ( 
	id                   integer DEFAULT nextval('demande_rechargement_id_seq'::regclass) NOT NULL  ,
	id_user              integer  NOT NULL  ,
	montant              real  NOT NULL  ,
	"date"               date  NOT NULL  ,
	status               integer  NOT NULL  ,
	CONSTRAINT pk_demande_rechargement PRIMARY KEY ( id )
 );

CREATE  TABLE "public".enchere ( 
	id                   integer DEFAULT nextval('produit_enchere_id_seq'::regclass) NOT NULL  ,
	date_debut           timestamp  NOT NULL  ,
	description          text    ,
	id_categorie         integer  NOT NULL  ,
	prix_minimal_vente   real  NOT NULL  ,
	status               integer  NOT NULL  ,
	comission            real  NOT NULL  ,
	duree                real  NOT NULL  ,
	id_user              integer  NOT NULL  ,
	CONSTRAINT pk_produit_enchere PRIMARY KEY ( id )
 );

CREATE  TABLE "public".mise_enchere ( 
	id                   integer DEFAULT nextval('mise_enchere_id_seq'::regclass) NOT NULL  ,
	id_enchere           integer  NOT NULL  ,
	id_user              integer  NOT NULL  ,
	montant              integer  NOT NULL  ,
	"date"               date  NOT NULL  ,
	CONSTRAINT pk_mise_enchere PRIMARY KEY ( id )
 );

CREATE  TABLE "public".mouvement_compte ( 
	id                   integer DEFAULT nextval('mouvement_compte_id_seq'::regclass) NOT NULL  ,
	id_user              integer  NOT NULL  ,
	id_type_mouvement    integer  NOT NULL  ,
	montant              integer  NOT NULL  ,
	CONSTRAINT pk_mouvement_compte PRIMARY KEY ( id )
 );

CREATE  TABLE "public".photos_enchere ( 
	id                   integer DEFAULT nextval('photos_enchere_id_seq'::regclass) NOT NULL  ,
	lien                 varchar  NOT NULL  ,
	id_enchere           integer  NOT NULL  ,
	CONSTRAINT pk_photos_enchere PRIMARY KEY ( id )
 );

ALTER TABLE "public".admin_token ADD CONSTRAINT fk_admin_token_admin FOREIGN KEY ( admin_id ) REFERENCES "public"."admin"( id );

ALTER TABLE "public".demande_rechargement ADD CONSTRAINT fk_demande_rechargement_user FOREIGN KEY ( id_user ) REFERENCES "public"."user"( id );

ALTER TABLE "public".enchere ADD CONSTRAINT fk_produit_enchere_categorie FOREIGN KEY ( id_categorie ) REFERENCES "public".categorie( id );

ALTER TABLE "public".enchere ADD CONSTRAINT fk_enchere_user FOREIGN KEY ( id_user ) REFERENCES "public"."user"( id );

ALTER TABLE "public".mise_enchere ADD CONSTRAINT fk_mise_enchere_enchere FOREIGN KEY ( id_enchere ) REFERENCES "public".enchere( id );

ALTER TABLE "public".mise_enchere ADD CONSTRAINT fk_mise_enchere_user FOREIGN KEY ( id_user ) REFERENCES "public"."user"( id );

ALTER TABLE "public".mouvement_compte ADD CONSTRAINT fk_mouvement_compte_type_mouvement FOREIGN KEY ( id_type_mouvement ) REFERENCES "public".type_mouvement( id );

ALTER TABLE "public".mouvement_compte ADD CONSTRAINT fk_mouvement_compte_user FOREIGN KEY ( id_user ) REFERENCES "public"."user"( id );

ALTER TABLE "public".photos_enchere ADD CONSTRAINT fk_photos_enchere_produit_enchere FOREIGN KEY ( id_enchere ) REFERENCES "public".enchere( id );

INSERT INTO "public"."admin"( id, nom, email, mdp ) VALUES ( default, 'Jean', 'jean@gmail.com', '12345');
INSERT INTO "public"."admin"( id, nom, email, mdp ) VALUES ( default, 'Jeanne', 'jr@yahoo.com', '123');
INSERT INTO "public"."admin"( id, nom, email, mdp ) VALUES ( default, 'Jeannot', 'kakashi@gmail.com', '1234');
INSERT INTO "public".admin_token( id, admin_id, "value", date_expiration ) VALUES ( default, 1, '1a9a98854b1cabd7fde873608362bb92b81d20ad', '2023-01-17 03:25:17 AM');
INSERT INTO "public".admin_token( id, admin_id, "value", date_expiration ) VALUES ( default, 1, '1b43247d6bd2faed525c4b832c67d0cb10018764', '2023-01-17 02:26:08 PM');
