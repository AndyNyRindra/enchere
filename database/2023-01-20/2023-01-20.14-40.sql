--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: admin_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_id_seq OWNER TO enchere;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.admin (
    id integer DEFAULT nextval('public.admin_id_seq'::regclass) NOT NULL,
    nom character varying NOT NULL,
    email character varying NOT NULL,
    mdp character varying NOT NULL
);


ALTER TABLE public.admin OWNER TO enchere;

--
-- Name: admin_0_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.admin_0_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_0_id_seq OWNER TO enchere;

--
-- Name: admin_token_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.admin_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_token_id_seq OWNER TO enchere;

--
-- Name: admin_token; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.admin_token (
    id integer DEFAULT nextval('public.admin_token_id_seq'::regclass) NOT NULL,
    admin_id integer NOT NULL,
    value character varying(255) NOT NULL,
    date_expiration timestamp without time zone NOT NULL
);


ALTER TABLE public.admin_token OWNER TO enchere;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO enchere;

--
-- Name: categorie; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.categorie (
    id integer DEFAULT nextval('public.category_id_seq'::regclass) NOT NULL,
    nom character varying NOT NULL
);


ALTER TABLE public.categorie OWNER TO enchere;

--
-- Name: commission_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.commission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commission_id_seq OWNER TO enchere;

--
-- Name: comission; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.comission (
    id integer DEFAULT nextval('public.commission_id_seq'::regclass) NOT NULL,
    valeur real NOT NULL,
    date date NOT NULL
);


ALTER TABLE public.comission OWNER TO enchere;

--
-- Name: demande_rechargement_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.demande_rechargement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.demande_rechargement_id_seq OWNER TO enchere;

--
-- Name: demande_rechargement; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.demande_rechargement (
    id integer DEFAULT nextval('public.demande_rechargement_id_seq'::regclass) NOT NULL,
    id_user integer NOT NULL,
    montant real NOT NULL,
    date date NOT NULL,
    status integer NOT NULL
);


ALTER TABLE public.demande_rechargement OWNER TO enchere;

--
-- Name: duree_defaut_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.duree_defaut_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.duree_defaut_id_seq OWNER TO enchere;

--
-- Name: duree_defaut; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.duree_defaut (
    id integer DEFAULT nextval('public.duree_defaut_id_seq'::regclass) NOT NULL,
    duree real NOT NULL,
    date date NOT NULL
);


ALTER TABLE public.duree_defaut OWNER TO enchere;

--
-- Name: produit_enchere_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.produit_enchere_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.produit_enchere_id_seq OWNER TO enchere;

--
-- Name: enchere; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.enchere (
    id integer DEFAULT nextval('public.produit_enchere_id_seq'::regclass) NOT NULL,
    date_debut timestamp without time zone NOT NULL,
    description text,
    id_categorie integer NOT NULL,
    prix_minimal_vente real NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    comission real NOT NULL,
    duree real NOT NULL,
    id_user integer NOT NULL,
    nom character varying,
    date_fin timestamp without time zone
);


ALTER TABLE public.enchere OWNER TO enchere;

--
-- Name: mise_enchere_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.mise_enchere_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mise_enchere_id_seq OWNER TO enchere;

--
-- Name: mise_enchere; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.mise_enchere (
    id integer DEFAULT nextval('public.mise_enchere_id_seq'::regclass) NOT NULL,
    id_enchere integer NOT NULL,
    id_user integer NOT NULL,
    montant integer NOT NULL,
    date timestamp without time zone NOT NULL,
    est_plus_haut boolean
);


ALTER TABLE public.mise_enchere OWNER TO enchere;

--
-- Name: mouvement_compte_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.mouvement_compte_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mouvement_compte_id_seq OWNER TO enchere;

--
-- Name: mouvement_compte; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.mouvement_compte (
    id integer DEFAULT nextval('public.mouvement_compte_id_seq'::regclass) NOT NULL,
    id_user integer NOT NULL,
    id_type_mouvement integer NOT NULL,
    montant integer NOT NULL
);


ALTER TABLE public.mouvement_compte OWNER TO enchere;

--
-- Name: photos_enchere_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.photos_enchere_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.photos_enchere_id_seq OWNER TO enchere;

--
-- Name: photos_enchere; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.photos_enchere (
    id integer DEFAULT nextval('public.photos_enchere_id_seq'::regclass) NOT NULL,
    lien character varying NOT NULL,
    id_enchere integer NOT NULL
);


ALTER TABLE public.photos_enchere OWNER TO enchere;

--
-- Name: type_mouvement_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.type_mouvement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_mouvement_id_seq OWNER TO enchere;

--
-- Name: type_mouvement; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.type_mouvement (
    id integer DEFAULT nextval('public.type_mouvement_id_seq'::regclass) NOT NULL,
    type character varying NOT NULL
);


ALTER TABLE public.type_mouvement OWNER TO enchere;

--
-- Name: user; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public."user" (
    id integer DEFAULT nextval('public.admin_0_id_seq'::regclass) NOT NULL,
    nom character varying NOT NULL,
    email character varying NOT NULL,
    mdp character varying NOT NULL
);


ALTER TABLE public."user" OWNER TO enchere;

--
-- Name: user_token; Type: TABLE; Schema: public; Owner: enchere
--

CREATE TABLE public.user_token (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    value character varying(255) NOT NULL,
    date_expiration timestamp without time zone DEFAULT now()
);


ALTER TABLE public.user_token OWNER TO enchere;

--
-- Name: user_token_id_seq; Type: SEQUENCE; Schema: public; Owner: enchere
--

CREATE SEQUENCE public.user_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_token_id_seq OWNER TO enchere;

--
-- Name: user_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enchere
--

ALTER SEQUENCE public.user_token_id_seq OWNED BY public.user_token.id;


--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.admin (id, nom, email, mdp) FROM stdin;
1	Jean	jean@gmail.com	12345
2	Jeanne	jr@yahoo.com	123
3	Jeannot	kakashi@gmail.com	1234
\.


--
-- Data for Name: admin_token; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.admin_token (id, admin_id, value, date_expiration) FROM stdin;
1	1	1a9a98854b1cabd7fde873608362bb92b81d20ad	2023-01-17 03:25:17
2	1	1b43247d6bd2faed525c4b832c67d0cb10018764	2023-01-17 14:26:08
3	1	486ced482ad2ad916aac81104d1d88895d45dc64	2023-01-18 16:53:35
\.


--
-- Data for Name: categorie; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.categorie (id, nom) FROM stdin;
1	Nouriture
2	Nettoyage
3	Voiture
\.


--
-- Data for Name: comission; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.comission (id, valeur, date) FROM stdin;
1	20	2023-01-18
\.


--
-- Data for Name: demande_rechargement; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.demande_rechargement (id, id_user, montant, date, status) FROM stdin;
\.


--
-- Data for Name: duree_defaut; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.duree_defaut (id, duree, date) FROM stdin;
1	72	2023-01-18
\.


--
-- Data for Name: enchere; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.enchere (id, date_debut, description, id_categorie, prix_minimal_vente, status, comission, duree, id_user, nom, date_fin) FROM stdin;
1	2023-01-20 00:00:00	Pack de yaourt yoplait a un prix imbattable	1	3000	0	600	24	1	Pack yaourt	2023-01-21 00:00:00
2	2023-01-19 00:00:00	Tesla V8 nouvelle generation full electrique	3	5e+07	0	1e+07	72	1	Tesla V8	2023-01-22 00:00:00
\.


--
-- Data for Name: mise_enchere; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.mise_enchere (id, id_enchere, id_user, montant, date, est_plus_haut) FROM stdin;
1	1	2	4000	2023-01-20 00:00:00	f
2	1	3	5000	2023-01-20 08:08:55	t
3	2	3	55000000	2023-01-20 08:34:26	f
4	2	2	65000000	2023-01-20 08:35:16	t
\.


--
-- Data for Name: mouvement_compte; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.mouvement_compte (id, id_user, id_type_mouvement, montant) FROM stdin;
\.


--
-- Data for Name: photos_enchere; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.photos_enchere (id, lien, id_enchere) FROM stdin;
\.


--
-- Data for Name: type_mouvement; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.type_mouvement (id, type) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public."user" (id, nom, email, mdp) FROM stdin;
1	andy	andy@gmail.com	1234
2	boost	boost@gmail.com	1234
3	jack	jack@gmail.com	1234
\.


--
-- Data for Name: user_token; Type: TABLE DATA; Schema: public; Owner: enchere
--

COPY public.user_token (id, user_id, value, date_expiration) FROM stdin;
1	1	ffev3zfv1e5rtv13sztre5zv	2023-01-20 14:34:03.984269
\.


--
-- Name: admin_0_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.admin_0_id_seq', 3, true);


--
-- Name: admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.admin_id_seq', 1, false);


--
-- Name: admin_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.admin_token_id_seq', 3, true);


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.category_id_seq', 3, true);


--
-- Name: commission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.commission_id_seq', 1, true);


--
-- Name: demande_rechargement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.demande_rechargement_id_seq', 1, false);


--
-- Name: duree_defaut_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.duree_defaut_id_seq', 1, true);


--
-- Name: mise_enchere_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.mise_enchere_id_seq', 4, true);


--
-- Name: mouvement_compte_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.mouvement_compte_id_seq', 1, false);


--
-- Name: photos_enchere_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.photos_enchere_id_seq', 1, false);


--
-- Name: produit_enchere_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.produit_enchere_id_seq', 2, true);


--
-- Name: type_mouvement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.type_mouvement_id_seq', 1, false);


--
-- Name: user_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enchere
--

SELECT pg_catalog.setval('public.user_token_id_seq', 1, true);


--
-- Name: admin_token pk_admin_token; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.admin_token
    ADD CONSTRAINT pk_admin_token PRIMARY KEY (id);


--
-- Name: categorie pk_category; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT pk_category PRIMARY KEY (id);


--
-- Name: comission pk_commission; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.comission
    ADD CONSTRAINT pk_commission PRIMARY KEY (id);


--
-- Name: demande_rechargement pk_demande_rechargement; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.demande_rechargement
    ADD CONSTRAINT pk_demande_rechargement PRIMARY KEY (id);


--
-- Name: duree_defaut pk_duree_defaut; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.duree_defaut
    ADD CONSTRAINT pk_duree_defaut PRIMARY KEY (id);


--
-- Name: mise_enchere pk_mise_enchere; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.mise_enchere
    ADD CONSTRAINT pk_mise_enchere PRIMARY KEY (id);


--
-- Name: mouvement_compte pk_mouvement_compte; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.mouvement_compte
    ADD CONSTRAINT pk_mouvement_compte PRIMARY KEY (id);


--
-- Name: photos_enchere pk_photos_enchere; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.photos_enchere
    ADD CONSTRAINT pk_photos_enchere PRIMARY KEY (id);


--
-- Name: enchere pk_produit_enchere; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.enchere
    ADD CONSTRAINT pk_produit_enchere PRIMARY KEY (id);


--
-- Name: admin pk_tbl; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT pk_tbl PRIMARY KEY (id);


--
-- Name: user pk_tbl_0; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT pk_tbl_0 PRIMARY KEY (id);


--
-- Name: type_mouvement pk_type_mouvement; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.type_mouvement
    ADD CONSTRAINT pk_type_mouvement PRIMARY KEY (id);


--
-- Name: user_token user_token_pkey; Type: CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.user_token
    ADD CONSTRAINT user_token_pkey PRIMARY KEY (id);


--
-- Name: admin_token fk_admin_token_admin; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.admin_token
    ADD CONSTRAINT fk_admin_token_admin FOREIGN KEY (admin_id) REFERENCES public.admin(id);


--
-- Name: demande_rechargement fk_demande_rechargement_user; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.demande_rechargement
    ADD CONSTRAINT fk_demande_rechargement_user FOREIGN KEY (id_user) REFERENCES public."user"(id);


--
-- Name: enchere fk_enchere_user; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.enchere
    ADD CONSTRAINT fk_enchere_user FOREIGN KEY (id_user) REFERENCES public."user"(id);


--
-- Name: mise_enchere fk_mise_enchere_enchere; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.mise_enchere
    ADD CONSTRAINT fk_mise_enchere_enchere FOREIGN KEY (id_enchere) REFERENCES public.enchere(id);


--
-- Name: mise_enchere fk_mise_enchere_user; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.mise_enchere
    ADD CONSTRAINT fk_mise_enchere_user FOREIGN KEY (id_user) REFERENCES public."user"(id);


--
-- Name: mouvement_compte fk_mouvement_compte_type_mouvement; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.mouvement_compte
    ADD CONSTRAINT fk_mouvement_compte_type_mouvement FOREIGN KEY (id_type_mouvement) REFERENCES public.type_mouvement(id);


--
-- Name: mouvement_compte fk_mouvement_compte_user; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.mouvement_compte
    ADD CONSTRAINT fk_mouvement_compte_user FOREIGN KEY (id_user) REFERENCES public."user"(id);


--
-- Name: photos_enchere fk_photos_enchere_produit_enchere; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.photos_enchere
    ADD CONSTRAINT fk_photos_enchere_produit_enchere FOREIGN KEY (id_enchere) REFERENCES public.enchere(id);


--
-- Name: enchere fk_produit_enchere_categorie; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.enchere
    ADD CONSTRAINT fk_produit_enchere_categorie FOREIGN KEY (id_categorie) REFERENCES public.categorie(id);


--
-- Name: user_token user_token_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: enchere
--

ALTER TABLE ONLY public.user_token
    ADD CONSTRAINT user_token_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

