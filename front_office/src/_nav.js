import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilApps, cilBasket,
  cilBell,
  cilCalculator,
  cilChartPie, cilChevronDoubleDown, cilClock,
  cilCursor,
  cilDescription,
  cilDrop, cilEuro, cilMeh,
  cilNotes,
  cilPencil,
  cilPuzzle,
  cilSpeedometer,
  cilStar
} from "@coreui/icons";
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Login',
    to: '/login',
    icon: <CIcon icon={cilMeh} customClassName="nav-icon" />,

  },
  {
    component: CNavItem,
    name: 'Enchères',
    to: '/encheres',
    icon: <CIcon icon={cilApps} customClassName="nav-icon" />,

  },
  {
    component: CNavItem,
    name: 'Mes Enchères',
    to: '/encheres/historique',
    icon: <CIcon icon={cilApps} customClassName="nav-icon" />,

  },

]

export default _nav
