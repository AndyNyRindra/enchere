import React from 'react'
import { CFormFeedback, CFormInput, CInputGroup, CInputGroupText } from "@coreui/react";


const MyInput = (props) => {

  return (
    <>

      <CInputGroup className={props.class}>
        <CInputGroupText id="basic-addon1">{props.name}</CInputGroupText>
        <CFormInput placeholder={props.label} aria-label={props.label} aria-describedby="basic-addon1" id={props.name} name={props.name} type={props.type}/>
        <CFormFeedback invalid>{props.invalid}</CFormFeedback>
      </CInputGroup>
    </>
  )
}

export default MyInput
