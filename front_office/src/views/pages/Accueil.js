import React from 'react'
import {useState} from 'react';
import {useEffect} from 'react';
import {CButton, CCol, CRow, CTable} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {cilPlus} from "@coreui/icons";
import { Link } from "react-router-dom";


const Accueil = () => {

  return (
    <>
      <ul>
        <li><Link to="/categories">Catégories</Link></li>
        <li><Link to="/comissions">Comissions</Link></li>
        <li><Link to="/dureeEncheres">Durées</Link></li>
        <li><Link to="/login">Login</Link></li>
      </ul>

    </>
  )
}

export default Accueil
