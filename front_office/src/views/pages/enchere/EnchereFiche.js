import React from 'react'
import {useState} from 'react';
import {useEffect} from 'react';
import {CCol, CRow, CTable} from "@coreui/react";


const EnchereFiche = () => {
  const [encheres, setEncheres] = useState([]);
  if (token == null) {
    window.location.href = '/login'
  }
  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  }
  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/enchere', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setEncheres(res.data)
        }
      })
  }, [])


  const columns = [
    {
      key: 'id',
      label: '#',
      _props: { scope: 'col' },
    },
    {
      key: 'nom',
      label: 'Nom',
      _props: { scope: 'col' },
    }
  ]
  const items = []
  function getOneLine(enchere) {
    const item = {
      id: enchere.id,
      nom: enchere.nom,
      _cellProps: { id: { scope: 'row' } },

    }
    items.push(item)
  }
  encheres.map(getOneLine)
  return (
    <>
      <CRow>
        <CCol><h2>Fiche Enchère</h2></CCol>
        <CCol></CCol>
      </CRow>


    </>
  )
}

export default EnchereFiche
