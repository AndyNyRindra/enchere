import React from 'react'
import {useState} from 'react';
import {useEffect} from 'react';
import {CButton, CCol, CForm, CFormCheck, CFormLabel, CFormSelect, CRow, CTable} from "@coreui/react";
import MyInput from "../../forms/MyInput";
import {Link} from "react-router-dom";


const EnchereHistorique = () => {
  const [encheres, setEncheres] = useState([]);


  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'user_token': sessionStorage.getItem("user_token")
    }
  }

  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/enchere/mine', content).then(res => res.json())
      .then(res => {
        if (res.code === 401) {
          window.location.replace('/login')
        } else if (res.data != null) {
          setEncheres(res.data)
        }
      })
  }, [])


  const columns = [
    {
      key: 'id',
      label: '#',
      _props: { scope: 'col' },
    },
    {
      key: 'nom',
      label: 'Nom',
      _props: { scope: 'col' },
    },
    {
      key: 'dateDebut',
      label: 'debut',
      _props: { scope: 'col' },
    },
    {
      key: 'dateFin',
      label: 'fin',
      _props: { scope: 'col' },
    },
    {
      key: 'status',
      label: 'status',
      _props: { scope: 'col' },
    },
    {
      key: 'prixMinimalVente',
      label: 'prix Minimal de Vente',
      _props: { scope: 'col' },
    },
    {
      key: 'button',
      label: '',
      _props: { scope: 'col' },
    }
  ]
  const items = []
  function getOneLine(enchere) {
    const item = {
      id: enchere.id,
      nom: enchere.nom,
      dateDebut: new Date(enchere.dateDebut).toLocaleString(),
      dateFin: new Date(enchere.dateFin).toLocaleString(),
      status: enchere.status===0?'non-commencé': enchere.status === 1?'en-cours': enchere.status===10?'finis':'-',
      prixMinimalVente: enchere.prixMinimalVente,
      button: <Link to={`/encheres/${enchere.id}`}><CButton
        color= "success">
        Voir détails
      </CButton></Link>,
      _cellProps: { id: { scope: 'row' } },

    }
    items.push(item)
  }
  encheres.map(getOneLine)

  return (
    <>
      <CRow>
        <CCol><h2>Historique de mes enchères</h2></CCol>
        <CCol></CCol>
      </CRow>

      <CTable striped columns={columns} items={items} />

    </>
  )
}

export default EnchereHistorique
