import React from 'react'
import {useState} from 'react';
import {useEffect} from 'react';
import {CButton, CCol, CForm, CFormCheck, CFormLabel, CFormSelect, CRow, CTable} from "@coreui/react";
import MyInput from "../../forms/MyInput";
import {Link} from "react-router-dom";


const EnchereList = () => {
  const [encheres, setEncheres] = useState([]);
  const [categories, setCategories] = useState([]);
  const [validated, setValidated] = useState(false)

  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  }

  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/categories', content).then(res => res.json())
      .then(res => {
        setCategories(res.data)
      })
  }, [])


  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/enchere', content).then(res => res.json())
      .then(res => {
        if (res.data != null) {
          setEncheres(res.data)
        }
      })
  }, [])


  const columns = [
    {
      key: 'id',
      label: '#',
      _props: { scope: 'col' },
    },
    {
      key: 'nom',
      label: 'Nom',
      _props: { scope: 'col' },
    },
    {
      key: 'dateDebut',
      label: 'debut',
      _props: { scope: 'col' },
    },
    {
      key: 'dateFin',
      label: 'fin',
      _props: { scope: 'col' },
    },
    {
      key: 'status',
      label: 'status',
      _props: { scope: 'col' },
    },
    {
      key: 'prixMinimalVente',
      label: 'prix Minimal de Vente',
      _props: { scope: 'col' },
    },
    {
      key: 'button',
      label: '',
      _props: { scope: 'col' },
    }
  ]
  const items = []
  function getOneLine(enchere) {
    const item = {
      id: enchere.id,
      nom: enchere.nom,
      dateDebut: new Date(enchere.dateDebut).toLocaleString(),
      dateFin: new Date(enchere.dateFin).toLocaleString(),
      status: enchere.status===0?'non-commencé': enchere.status === 1?'en-cours': enchere.status===10?'finis':'-',
      prixMinimalVente: enchere.prixMinimalVente,
      button: <Link to={`/encheres/${enchere.id}`}><CButton
        color= "success">
        Voir détails
      </CButton></Link>,
      _cellProps: { id: { scope: 'row' } },

    }
    items.push(item)
  }
  encheres.map(getOneLine)

  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    } else {

      const content2 = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        }
      };
      const checkedCategories = Array.from(document.querySelectorAll('input[name="categories"]:checked'))
        .map(input => input.value)
      content2.body = JSON.stringify(
        {
                motCle: event.target.motCle.value,
                dateMin: event.target.dateMin.value,
                dateMax: event.target.dateMax.value,
                categories: checkedCategories,
                prixMin: event.target.prixMin.value,
                prixMax: event.target.prixMax.value,
                status: event.target.status.value,
              }
      )



      console.log(content2.body)
      fetch('https://enchereandy-production.up.railway.app/enchere/recherche', content2).then(response => response.json()).then((data) => {
        if (data.code === 400) {
          alert(data.message);
          event.preventDefault()
          event.stopPropagation()
        }
        else if (data.data != null) {
          setEncheres(data.data)
        }
      }).catch(e => {
        alert(e)
        event.preventDefault()
        event.stopPropagation()
      })
    }
    setValidated(true)
    event.preventDefault()
    event.stopPropagation()

  }
  return (
    <>
      <h2>Rechercher des enchères</h2>
      <CForm onSubmit={handleSubmit} noValidate
             validated={validated} method="GET">

        <MyInput name="motCle" label="Mot Cle" type="text" class="mb-3"/>
        <CRow>
          <CCol><MyInput name="prixMin" label="Prix Min" type="number" class="mb-3"/></CCol>
          <CCol><MyInput name="prixMax" label="Prix Max" type="number" class="mb-3"/></CCol>
        </CRow>
        <CRow>
          <CCol><MyInput name="dateMin" label="Date Min" type="datetime-local" class="mb-3"/></CCol>
          <CCol><MyInput name="dateMax" label="Date Max" type="datetime-local" class="mb-3"/></CCol>
        </CRow>
        <CFormLabel className="mb-3">Status</CFormLabel>
        <CFormSelect size="sm" className="mb-3" aria-label="Status" id="status" name="status">
          <option value="">...</option>
          <option value="0">Non commencé</option>
          <option value="1">En cours</option>
          <option value="10">Terminé</option>
        </CFormSelect>
        <CFormLabel className="mb-3">Catégories</CFormLabel>
        <div style={{ display: "grid", gridTemplateColumns: "1fr 1fr 1fr 1fr", gridGap: "20px" }}>
          {categories.map((categorie, index) => (
            <CFormCheck key={index} id="categories" label={categorie.nom} name="categories" value={categorie.id} />
          ))}
        </div>

        <CButton color="primary" className="px-4" type="submit">
          Rechercher
        </CButton>
      </CForm>
      <CRow>
        <CCol><h2>Liste des enchères</h2></CCol>
        <CCol></CCol>
      </CRow>

      <CTable striped columns={columns} items={items} />

    </>
  )
}

export default EnchereList
