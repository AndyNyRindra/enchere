import React, {useState} from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm, CFormFeedback,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'

const Login = () => {
  const [validated, setValidated] = useState(false)

  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    } else {

      const email = event.target.email.value;
      const password = event.target.password.value;

      const content = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      };

      content.body = JSON.stringify({email: email, mdp: password})
      fetch('https://enchereandy-production.up.railway.app/enchere', content).then(response => response.json()).then((data) => {
        event.preventDefault()
        event.stopPropagation()
        console.log(data)
        if (data.code === 401) {
          alert("error");
          event.preventDefault()
          event.stopPropagation()
        } else if (data.data != null) {
          mapToHome(data.data.value)
        }
      }).catch(e => {
        event.preventDefault()
        event.stopPropagation()
      })
    }
    setValidated(true)
    event.preventDefault()
    event.stopPropagation()

  }

  function mapToHome(token) {
    sessionStorage.setItem("user_token", token)
    window.location.replace('/encheres')
  }


  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={handleSubmit} noValidate
                         validated={validated} method="POST">
                    <h1>Rencherir</h1>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser}/>
                      </CInputGroupText>
                      <CFormInput id="email" name="mise" placeholder="Mise" required/>
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton color="primary" className="px-4" type="submit">
                          Miser
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
