import {CHeader, CListGroup, CListGroupItem, CFormInput, CButton, CTable} from "@coreui/react";
import {Link, useParams} from "react-router-dom"
import React ,{ useState,useRef, useEffect } from "react";

const FicheEnchere = () => {
    let { id } = useParams();
    const [enchere, setEnchere] = useState([]);
    const refValue = useRef(null);

  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  }

  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/enchere/' + id, content).then(res => res.json())
      .then(res => {
        if (res.data != null) {
          setEnchere(res.data)
        }
      })
  }, [])

    function miser() {
    var content = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'user_token': sessionStorage.getItem("user_token")
            },
            body: JSON.stringify({
                "montant": refValue.current.value
            })
        }
        fetch('https://enchereandy-production.up.railway.app/enchere/' + id + '/mise', content).then(res => res.json())
            .then(res => {
                if (res.code === 401) {
                    window.location.replace('/login')
                } else if (res.code === 400) {
                  alert(res.message)
                } else if (res.data != null) {
                    window.location.reload()
                }
            })
    }

    function maptoHome(){
        window.location.href='/'
    }

    function mapToLogin() {
        // sessionStorage.setItem("token", token)
        window.location.href = '/login'
    }

  const columns = [
    {
      key: 'id',
      label: '#',
      _props: { scope: 'col' },
    },
    {
      key: 'user',
      label: 'Utilisateur',
      _props: { scope: 'col' },
    },
    {
      key: 'montant',
      label: 'Montant',
      _props: { scope: 'col' },
    },
    {
      key: 'date',
      label: 'Date',
      _props: { scope: 'col' },
    }

  ]
  const items = []
  function getOneLine(mise) {
    const item = {
      id: mise.id,
      user: mise.user.nom,
      montant: mise.montant,
      date: new Date(mise.date).toLocaleString(),
      _cellProps: { id: { scope: 'row' } },

    }
    items.push(item)
  }
  enchere.mises?.map(getOneLine)


    return (
        <>
            <CListGroup>
            <CListGroupItem active><b>Nom du produit: </b>{enchere.nom}</CListGroupItem>
                <CListGroupItem><b>Description: </b>{enchere.description}</CListGroupItem>
                <CListGroupItem><b>Categorie: </b>{enchere.categorie?.nom}</CListGroupItem>
                <CListGroupItem><b>Date debut: </b>{new Date(enchere.dateDebut).toLocaleString()}</CListGroupItem>
                <CListGroupItem><b>Date fin: </b>{new Date(enchere.dateFin).toLocaleString()}</CListGroupItem>
                <CListGroupItem><b>Prix minimal: </b>{enchere.prixMinimalVente}</CListGroupItem>
              <CListGroupItem><b>Status: </b>{enchere.status === 0 ? 'Non commencé' : enchere.status === 1 ? 'En cours' : 'Terminé'}</CListGroupItem>
            </CListGroup>
            {enchere.status===1?
            <>
            <h2>Rencherir</h2>
            <CFormInput ref={refValue} defaultValue='0' placeholder="Miser" />
            <CButton onClick={miser} color="primary" className="px-4">
                                Miser
            </CButton>

            </>
            :''}
            <CHeader className="d-flex justify-content-between">
                <h3>Liste des mises</h3>
            </CHeader>
          <CTable striped columns={columns} items={items} />
            </>
    )
}

export default FicheEnchere;
