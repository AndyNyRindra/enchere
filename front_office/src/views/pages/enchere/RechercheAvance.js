import React, {useEffect, useState} from "react";
import {CButton, CCol, CForm, CFormCheck, CFormLabel, CFormSelect, CRow} from "@coreui/react";
import MyInput from "../../forms/MyInput";


const RechercheAvance = () => {

  const [validated, setValidated] = useState(false)
  const [categories, setCategories] = useState([]);

  var content = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  }
  useEffect(() => {
    fetch('https://enchereandy-production.up.railway.app/categories', content).then(res => res.json())
      .then(res => {
          setCategories(res.data)
      })
  }, [])

  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    } else {
      const valeur = event.target.valeur.value;

      const content = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'admin_token': sessionStorage.getItem("admin_token")
        }
      };
      content.body = JSON.stringify({nom: valeur})
      fetch('https://enchereandy-production.up.railway.app/enchere/recherche', content).then(response => response.json()).then((data) => {
        if (data.code === 401) {
          alert("Please Login");
          window.location.replace('/login')
        } else if (data.code === 400) {
          alert("error");
          event.preventDefault()
          event.stopPropagation()
        }
        else if (data.data != null) {
          window.location.replace('/categories')
        }
      }).catch(e => {
        event.preventDefault()
        event.stopPropagation()
      })
    }
    setValidated(true)
    event.preventDefault()
    event.stopPropagation()

  }

  return (
    <>
      <h2>Rechercher des enchères</h2>
      <CForm onSubmit={handleSubmit} noValidate
             validated={validated} method="GET">

        <MyInput name="motCle" label="Mot Cle" type="text" class="mb-3"/>
        <CRow>
          <CCol><MyInput name="prixMin" label="Prix Min" type="number" class="mb-3"/></CCol>
          <CCol><MyInput name="prixMax" label="Prix Max" type="number" class="mb-3"/></CCol>
        </CRow>
        <CRow>
          <CCol><MyInput name="dateMin" label="Date Min" type="datetime-local" class="mb-3"/></CCol>
          <CCol><MyInput name="dateMax" label="Date Max" type="datetime-local" class="mb-3"/></CCol>
        </CRow>
        <CFormSelect size="sm" className="mb-3" aria-label="Small select example">
          <option>Status</option>
          <option value="0">Non commencé</option>
          <option value="1">En cours</option>
          <option value="10">Terminé</option>
        </CFormSelect>
        <CFormLabel className="mb-3">Catégories</CFormLabel>
        <div style={{ display: "grid", gridTemplateColumns: "1fr 1fr 1fr 1fr", gridGap: "20px" }}>
          {categories.map((categorie, index) => (
            <CFormCheck key={index} id={categorie.nom} label={categorie.nom} name="categories" value={categorie.id} />
          ))}
        </div>

        <CButton color="primary" className="px-4" type="submit">
          Rechercher
        </CButton>
      </CForm>
    </>
  )
}

export default RechercheAvance
