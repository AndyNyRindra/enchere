import { IonApp, IonRouterOutlet, IonSplitPane, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Redirect, Route } from 'react-router-dom';
import Menu from './components/Menu';
import Page from './pages/Page';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import EnchereForm from "./pages/EnchereForm";

import OneSignal from "onesignal-cordova-plugin";
import axios from "axios";
import {useEffect, useState} from "react";
import {Categorie} from "./models/Categorie";
import {Enchere} from "./models/Enchere";
import Login from "./pages/Login";
import EnchereList from "./pages/EnchereList";
import Inscription from "./pages/Inscription"
import Rechargement from "./pages/Rechargement";

setupIonicReact();

const App: React.FC = () => {

  // const OneSignalInit = () => {
  //   OneSignal.setAppId("f9260d5d-5ce4-4390-8999-83c953db8339");
  //   OneSignal.setNotificationOpenedHandler(function(jsonData) {
  //     console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
  //   })
  // }
  // OneSignalInit();



  const appId = "f9260d5d-5ce4-4390-8999-83c953db8339";
  const apiKey = "Zjc1OWY2MTgtOThmMy00ZGQ2LTljZWYtZTIzNmEzYjliYmMy";


  const [encheres, setEncheres] = useState<Enchere[]>([]);
  var content2 = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'}
  }
  useEffect(() => {
    const intervalId = setInterval(() => {
      fetch('https://enchereandy-production.up.railway.app/enchere/check', content2).then(res => res.json())
          .then(res => {
            setEncheres(res.data)
          })
    }, 60000); // call the function every 60 seconds (60000 milliseconds)

    return () => {
      clearInterval(intervalId);
    };
  }, []);


  for (let i = 0 ; i < encheres.length ; i++) {
      const content = {
        en: "L'enchère sur " + encheres[i].nom + " est terminée !",
      };
      axios.post(`https://onesignal.com/api/v1/notifications`, {
        app_id: appId,
        contents: content,
        included_segments: ["All"]
      }, {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Basic ${apiKey}`
        }
      })
          .then((response: { data: any; }) => {
            console.log(response.data);
          })
          .catch((error: any) => {
            console.error(error);
            alert(error)
          });
    }







  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
            <Route path="/" exact={true}>
              <Redirect to="/login" />
            </Route>
            <Route path="/page/:name" exact={true}>
              <Page />
            </Route>
            <Route path="/enchere/new" exact={true}>
              <EnchereForm />
            </Route>
              <Route path="/login" exact={true}>
                  <Login />
              </Route>
              <Route path="/encheres" exact={true}>
                  <EnchereList />
                </Route>
              <Route path="/inscription" exact={true}>
                  <Inscription />
              </Route>
              <Route path="/rechargement" exact={true}>
                  <Rechargement />
              </Route>
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
