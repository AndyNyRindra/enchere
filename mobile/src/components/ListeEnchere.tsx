import { IonHeader, IonToolbar, IonButtons, IonMenuButton, IonTitle, IonCol, IonContent, IonGrid, IonPage, IonRow } from '@ionic/react'
import { Enchere } from '../models/Enchere'
import EnchereItem from '../pages/EnchereItem';
import PageHeader from "./PageHeader";
import React from "react";


type Props = {
    enchere: Enchere[]
}

const ListeEnchere = (props: Props) => {
    return (
        <IonPage>
            <PageHeader name="Enchères" />
            <IonContent fullscreen>
  
            <IonGrid fixed>
              <IonRow>
                {props.enchere.map((enchere: Enchere) => (
                  <IonCol size="12" size-md="6" key={enchere.id}>
                    <EnchereItem
                      key={enchere.id}
                      enchere={enchere} />
                  </IonCol>
                ))}
              </IonRow>
            </IonGrid>
            </IonContent>
        </IonPage>
                
             
          );
       };


export default ListeEnchere