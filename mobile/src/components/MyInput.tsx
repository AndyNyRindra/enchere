import { IonItem, IonLabel, IonInput } from '@ionic/react';

import '../pages/Page.css';
import './Form.css';
import React, {RefObject} from "react";

const MyInput: React.FC<{ name: string, type: any, myRef: RefObject<HTMLIonInputElement> }> = ({name, type, myRef}) => {


    return (
        <IonItem className='inputitem'>
            <IonLabel position="floating">{name}</IonLabel>
            <IonInput type={type} ref={myRef}/>
        </IonItem>

    );
};

export default MyInput;
