export interface Enchere {
    id: number;
    nom: string;
    dateDebut: string;
    dateFin: string;
    prixMinimalVente: number;
    status: number;
}