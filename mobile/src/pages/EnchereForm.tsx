import {
    IonContent,
    IonLabel,
    IonButton,
    IonPage,
    IonItem,
    IonTextarea,
    IonSelect,
    IonSelectOption,
    IonList,
    IonActionSheet,
    IonGrid,
    IonRow,
    IonCol,
    IonImg,
    IonFab,
    IonFabButton,
    IonIcon, useIonAlert
} from '@ionic/react';

import './Page.css';
import PageHeader from "../components/PageHeader";
import React, {useEffect, useRef, useState} from "react";
import MyInput from "../components/MyInput";
import { camera, trash, close } from 'ionicons/icons';
import { usePhotoGallery, UserPhoto } from '../hooks/usePhotoGallery';
import {Categorie} from "../models/Categorie";
import {useHistory} from "react-router";

const EnchereForm: React.FC = () => {
    var nomRef = useRef<HTMLIonInputElement>(null);
    var descriRef = useRef<HTMLIonTextareaElement>(null);
    var dateDebutRef = useRef<HTMLIonInputElement>(null);
    var prixMinRef = useRef<HTMLIonInputElement>(null);
    var dureeRef = useRef<HTMLIonInputElement>(null);
    var catRef = useRef<HTMLIonSelectElement>(null);
    const [presentAlert] = useIonAlert();
    const history = useHistory();

    const { deletePhoto, photos, takePhoto } = usePhotoGallery();
    const [photoToDelete, setPhotoToDelete] = useState<UserPhoto>();

    const [categories, setCategories] = useState<Categorie[]>([]);

    var content = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'}
    }
    useEffect(() => {
        localStorage.removeItem("CapacitorStorage.photos");
        fetch('https://enchereandy-production.up.railway.app/categories', content).then(res => res.json())
            .then(res => {
                setCategories(res.data)
            })
    }, [])

    function saveEnchere() {

        const images = [];
        for (let i = 0; i < photos.length; i++) {
            const image = {
                bytes: photos[i].base64
            }
            images.push(image);
        }

        var token = sessionStorage.getItem('user_token');
        if (token == null) {
            token = ""
        }

        var content= {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "user_token": token
            },
            body: JSON.stringify(
            {
                    nom:nomRef.current?.value,
                    description:descriRef.current?.value,
                    dateDebut:dateDebutRef.current?.value,
                    categorie:{id:catRef.current?.value},
                    prixMinimalVente:prixMinRef.current?.value,
                    duree: dureeRef.current?.value,
                    photos: images
                }
            )
        }

        fetch('https://enchereandy-production.up.railway.app/enchere/new', content).then(response=>
            response.json()
        ).then((data)=>{
            console.log(data);
            if (data.code === 401) {
                presentAlert({
                    header: data.code,
                    message: data.message,
                    buttons: ['OK'],
                })
                window.location.replace("/login")
                // history.push('/login');
            } else if (data.code === 400) {
                presentAlert({
                    header: data.code,
                    message: data.message,
                    buttons: ['OK'],
                })
            }
            if (data.data == null) {
                presentAlert({
                    header: data.code,
                    message: data.message,
                    buttons: ['OK'],
                })
            } else {
                history.push('/encheres');
            }

        }).catch((error)=>{presentAlert({
            header: 'Echec',
            message: 'Echec creation enchere',
            buttons: ['OK'],
        })})
    }

    return (
        <IonPage>
            <PageHeader name="Nouvelle Enchère" />
            <IonContent fullscreen>

                <form className="ion-padding myform">
                    <IonLabel className='formtitle'>Que vendez - vous ?</IonLabel>
                    <MyInput name="Nom" type="text" myRef={nomRef} />
                    <IonItem className='inputitem'>
                        <IonLabel position="floating">Description</IonLabel>
                        <IonTextarea ref={descriRef} rows={5}/>
                    </IonItem>
                    <IonList>
                        <IonItem>
                            <IonSelect placeholder="Catégorie" ref={catRef}>
                                {categories?.map((categorie, index) => (
                                    <IonSelectOption value={categorie.id}>{categorie.nom}</IonSelectOption>
                                ))}

                            </IonSelect>
                        </IonItem>
                    </IonList>
                    <MyInput name="Date de départ" type="datetime-local" myRef={dateDebutRef} />
                    <MyInput name="Prix de départ" type="number" myRef={prixMinRef} />
                    <MyInput name="Durée en Heures" type="number" myRef={dureeRef} />

                <IonGrid>
                        <IonRow>
                            {photos.map((photo, index) => (
                                <IonCol size="6" key={index}>
                                    <IonImg onClick={() => setPhotoToDelete(photo)} src={photo.webviewPath} />

                                </IonCol>

                            ))}
                        </IonRow>


                        <IonButton onClick={() => takePhoto()}>
                            <IonIcon icon={camera}></IonIcon>
                        </IonButton>

                    <IonActionSheet
                        isOpen={!!photoToDelete}
                        buttons={[{
                            text: 'Delete',
                            role: 'destructive',
                            icon: trash,
                            handler: () => {
                                if (photoToDelete) {
                                    deletePhoto(photoToDelete);
                                    setPhotoToDelete(undefined);
                                }
                            }
                        }, {
                            text: 'Cancel',
                            icon: close,
                            role: 'cancel'
                        }]}
                        onDidDismiss={() => setPhotoToDelete(undefined)}
                    />
                </IonGrid>

                    <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={saveEnchere}>
                        Ajouter
                    </IonButton>
                </form>
            </IonContent>
        </IonPage>
    );
};

export default EnchereForm;
