import React from 'react';
import { Enchere } from '../models/Enchere' 
import { IonCard, IonCardHeader, IonItem, IonLabel, IonAvatar, IonCardContent, IonList } from '@ionic/react';

interface EnchereItemProps {
  enchere?: Enchere;
}

const EnchereItem: React.FC<EnchereItemProps> = ({enchere}) => {
  return (
    <>
      <IonCard className="speaker-card">
        <IonCardHeader>
          <IonItem button detail={false} lines="none" className="speaker-item" href={`/encheres/${enchere?.id}`}>
            <IonLabel>
              <h2>{enchere?.nom}</h2>
              <p>{enchere?.dateDebut}</p>
              <p>{enchere?.status===0?'non-commencé': enchere?.status === 1?'en-cours': enchere?.status===10?'finis':'-'}</p>
            </IonLabel>
          </IonItem>
        </IonCardHeader>

    
      </IonCard>
    </>
  );
};

export default EnchereItem;