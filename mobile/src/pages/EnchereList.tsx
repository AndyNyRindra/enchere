import  { useState, useEffect } from 'react';
import { Enchere } from '../models/Enchere';

import ListeEnchere from '../components/ListeEnchere';
import {useIonAlert} from "@ionic/react";
import {useHistory} from "react-router";


const EnchereList = () => {
    const [encheres, setEnchere] = useState<Enchere[]>([]);
    const [presentAlert] = useIonAlert();
    const history = useHistory();
    var token = sessionStorage.getItem('user_token');
    if (token == null) {
        token = ""
    }

    var content= {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            "user_token": token
        }
    }

    useEffect(() => {
       fetch('https://enchereandy-production.up.railway.app/enchere/mine', content)
          .then((response) => response.json())
          .then((data) => {
              if (data.code === 401) {
                  presentAlert({
                      header: data.code,
                      message: data.message,
                      buttons: ['OK'],
                  })
                  window.location.replace("/login")
                  // history.push('/login');
              }
             console.log(data);
             setEnchere(data.data);
          })
          .catch((err) => {
             console.log(err.message);
          });
    }, []);

    return (
      <ListeEnchere enchere={encheres}/>
              
           
        );
     };

     export default EnchereList;