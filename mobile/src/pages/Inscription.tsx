import { IonButton, IonCheckbox, IonContent, IonInput, IonItem, IonLabel, IonPage, useIonAlert } from '@ionic/react'
import React, { useRef } from 'react'
import { useHistory } from 'react-router';
import '../components/Form.css'
type Props = {}
import PageHeader from "../components/PageHeader";
const Form: React.FC = () => {
    const history = useHistory();
    var nameRef = useRef<HTMLIonInputElement>(null);
    var emailRef = useRef<HTMLIonInputElement>(null);
    var passwordRef = useRef<HTMLIonInputElement>(null);
    const [presentAlert] = useIonAlert();
    
    function register() {
        var name = nameRef.current?.value;
        var email = emailRef.current?.value;
        var password = passwordRef.current?.value;
        var content= {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({nom:name,email:email,mdp:password})
        }

        fetch('https://enchereandy-production.up.railway.app/user', content).then(response=>
            response.json()
        ).then((data)=>{
            console.log(data);
            if (data.data == null) {
              presentAlert({
                header: data.code,
                message: data.message,
                buttons: ['OK'],
              })
            } else {
              history.push('/login');
            }
            
        }).catch((error)=>{presentAlert({
            header: 'Sign up error',
            message: 'error during sign up has occuried',
            buttons: ['OK'],
        })})
    }

    return (
        <IonPage>
            <PageHeader name="Sign up" />
            <IonContent fullscreen>
                <form className="ion-padding myform">
            <IonLabel className='formtitle'>Sign up</IonLabel>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Your name</IonLabel>
                <IonInput ref={nameRef} />
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Your email</IonLabel>
                <IonInput ref={emailRef} />
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Password</IonLabel>
                <IonInput ref={passwordRef}/>
            </IonItem>
            <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={register}>
                sign up
            </IonButton>
        </form>
            </IonContent>
        </IonPage>
    )
}

export default Form