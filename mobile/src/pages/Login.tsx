import { IonButton, IonCheckbox, IonContent, IonInput, IonItem, IonLabel, IonPage, useIonAlert } from '@ionic/react'
import React, { useRef } from 'react'
import { useHistory } from 'react-router';
import '../components/Form.css'
type Props = {}
import PageHeader from "../components/PageHeader";
const Form: React.FC = () => {
    const history = useHistory();
    var emailRef = useRef<HTMLIonInputElement>(null);
    var passwordRef = useRef<HTMLIonInputElement>(null);
    const [presentAlert] = useIonAlert();
    
    function authentificate() {
        var email = emailRef.current?.value;
        var password = passwordRef.current?.value;
        var content= {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({email:email,mdp:password})
        }
        fetch('https://enchereandy-production.up.railway.app/user/login', content).then(response=>
            response.json()
        ).then((data)=>{
            console.log(data);
            if (data.data == null) {
              presentAlert({
                header: data.code,
                message: data.message,
                buttons: ['OK'],
              })
            } else {
              saveToken(data.data.value,'user_token');
              history.push('/enchere/new');
            }
            
        }).catch((error)=>{presentAlert({
            header: 'Sign in failed',
            message: 'Email or password not valid',
            buttons: ['OK'],
        })})
    }
    function saveToken(token:string, token_name:string) {
        sessionStorage.setItem(token_name, token);
    }
    return (
        <IonPage>
            <PageHeader name="Login" />
            <IonContent fullscreen>
                <form className="ion-padding myform">
            <IonLabel className='formtitle'>Login</IonLabel>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Your email</IonLabel>
                <IonInput ref={emailRef} value="jacob@gmail.com"/>
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Password</IonLabel>
                <IonInput ref={passwordRef} type="password" value="1234"/>
            </IonItem>
            <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={authentificate}>
                Login
            </IonButton>
        </form>
            </IonContent>
        </IonPage>
    )
}

export default Form