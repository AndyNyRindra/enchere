import { IonButton, IonCheckbox, IonContent, IonInput, IonItem, IonLabel, IonPage, useIonAlert } from '@ionic/react'
import React, { useRef } from 'react'
import { useHistory } from 'react-router';
import '../components/Form.css'
type Props = {}
import PageHeader from "../components/PageHeader";
import MyInput from "../components/MyInput";
const Rechargement: React.FC = () => {
    const history = useHistory();
    var montantRef = useRef<HTMLIonInputElement>(null);
    const [presentAlert] = useIonAlert();

    function recharger() {
        var montant = montantRef.current?.value;
        var content= {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'user_token': sessionStorage.getItem('user_token') || ''
            },
            body: JSON.stringify({montant:montant})
        }
        fetch('https://enchereandy-production.up.railway.app/rechargement/demande', content).then(response=>
            response.json()
        ).then((data)=>{
            console.log(data);
            if (data.code == 401) {
                presentAlert({
                    header: data.code,
                    message: data.message,
                    buttons: ['OK'],
                })
                window.location.replace("/login")
            }
            else if (data.data == null) {
                presentAlert({
                    header: data.code,
                    message: data.message,
                    buttons: ['OK'],
                })
            } else {
                window.location.reload()
            }

        }).catch((error)=>{presentAlert({
            header: 'Sign in failed',
            message: 'Email or password not valid',
            buttons: ['OK'],
        })})
    }

    return (
        <IonPage>
            <PageHeader name="Rechargement" />
            <IonContent fullscreen>
                <form className="ion-padding myform">
                    <MyInput name="Montant" type="number" myRef={montantRef} />

                    <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={recharger}>
                        Recharger
                    </IonButton>
                </form>
            </IonContent>
        </IonPage>
    )
}

export default Rechargement